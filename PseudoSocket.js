// JAVASCRIPT OBJECT
import axios from 'axios'

class PseudoSocket {
    constructor(host, options = {}){
        this._options = options;
        this._host = host;
        this._listeners = [];
        this.interval = null;
        
        // check options properties !
        if(options.delay === undefined){
            this._options = {
                ...options,
                delay: 1
            };
        }

        if(options.additional_data === undefined){
            this._options = {
                ...options,
                async: true,
                additional_data: {
                    session_key: '',
                }
            };
        }else{
            if(!this.isObject(options.additional_data)){
                throw 'PseudoSocket constructor options argument field additional_data must be instance is OBJECT!';
            }
             if(options.additional_data.session_key === undefined) {
                 this._options = {
                     ...options,
                     additional_data: {
                         ...options.additional_data,
                         session_key: ''
                     }
                 };
             }
        }

        if(this._host === undefined){
            throw 'PseudoSocket constructor argument host is required!';
        }

        if(options.send_event_url === undefined){
            throw 'PseudoSocket constructor options argument field send_event_url is required!';
        }

        if(options.fetch_event_url === undefined){
            throw 'PseudoSocket constructor options argument field fetch_event_url is required!';
        }
    };

    _start () {
        const _this = this;
        _this.fetch_events();

        this.interval = setInterval(function(){
            _this.fetch_events();
        }, _this._options.delay * 1000);
    };

    _stop() {
        if (this.interval != null){
            clearInterval(this.interval);
        }
    };

    fetch_events () {
        if(this._listeners.length == 0){
            return;
        }
        axios.post(this._host + this._options.fetch_event_url, this._options.additional_data)
            .then(res => {
                this._run_listeners(res);
            })
            .catch(err => console.log({
                msg: err.message,
                status: err.status
            }));
    };

    _connect (obj) {
        this.set_additional_value('session_key', obj.payload.session_key)
    };

    _disconnect (obj) {
        this.set_additional_value('session_key', '')
    };

    _run_event (obj) {
        if(obj.type == "CONNECT"){
            this._connect(obj)
        }

        if(obj.type == "DISCONNECT"){
            this._disconnect(obj)
        }

        for (let i = 0; i < this._listeners.length; i++){
            let event = this._listeners[i];
            if(event.type == obj.type){
                event.callback(obj);
                break;
            }
        }
    };

    _run_listeners (request) {
        request.data.map(obj => {
            this._run_event(obj);
        })
    };

    isObject (value) {
        return value && typeof value === 'object' && value.constructor === Object;
    }

    isFunction (value) {
        return typeof value === 'function';
    }

    on (type, callback = null) {
        for(let i=0; i < this._listeners.length; i++){
            let event = this._listeners[i];
            if(event.type == type){
                throw type + 'already is declared!'
            }
        }
        this._listeners.push({type: type, callback: callback});
    };

    emit (type, payload = {}, options = {}) {
        if(this.isFunction(options.on_before)){
            options.on_before();
        }

        let data = {
            type: type,
            payload: payload,
            async: !this.isFunction(options.on_emit),
            params: this._options.additional_data,
        }

        const response = axios.post(this._host + this._options.fetch_event_url, data)
            .then(res => {
                if(this.isFunction(options.on_emit)){
                    options.on_emit(res.data);
                }
            })
            .catch(err => {
                if(this.isFunction(options.on_error)){
                    options.on_error(err);
                }else{
                    throw err;
                }
            });
    };

    set_additional_value (key, value) {
        this._options.additional_data[key] = value;
    };

    get_additional_value (key) {
        return this._options.additional_data[key];
    };
}

export default PseudoSocket;