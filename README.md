# pseudo-socket

# installing
     Using npm:
     
    ```bash
    $ npm install axios
    $ npm install pseudo-socket
    ```

# Example (Sample)
    import PseudoSocket from 'pseudo-socket'

    ## creat socket object (*)
    const options = {
            delay: 3,
            send_event_url: '/api/v1/emit/',
            fetch_event_url: '/api/v1/events/',
        };
        
    const socket = new PseudoSocket('hostname', options)
    
    ## make action by action type (**) 
    socket.on('LOGIN', (data) => console.log(data))
    
    ### start socket (***)
    socket.start();

# Example (Sample) Explanation
    (*)   => class PseudoSocket accept two parameter (host, options):
          'host' => Project host name (For Example: 'http://127.0.0.1:8000')
          'options'  => consist of three properties (delay, send_event_url, fetch_event_url)
            'delay'           ==> Specifies the interval of the method _start()
            'send_event_url'  ==> Is a api url, which you will send emit to server with this api url 
            'fetch_event_url' ==> Is a api url, which you will get events from server with this api url
    (**)  => method on() accepted two parameter
           'type'     => This parameter characterization incoming type of data
           'callback' => This callback function execute mentioned operation by incoming type of data
    
    (***) => method start() is performed 'delay second' interval
    
# Example (Lifecycle)
    import PseudoSocket from 'pseudo-socket'

    ## creat socket object (*)
    class Foo extends Component {
        
        constructor(props){
            super(props)
            const options = {
                delay: 3,
                send_event_url : '/api/v1/emit',
                fetch_event)url : '/api/v1/events',
            }
        }
        
        
        componentDidMount() {
    
            this.socket.on('CONNECT', (data) => console.log(data, this.socket._options));
    
            this.socket.start();
    
            this.socket.set_additional_value('user_token', '8sj3maslm34_wsdfokjn4');
    
        };
        
        componentWillUnmount() {
            this.socket.stop(); (*)
        }
    }
    
 # Example (Sample) Explanation 
    stop() => Clear start() method interval, when unmount component Foo
    
 
